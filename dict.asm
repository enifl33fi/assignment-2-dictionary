%include "lib.inc"

%define ID_SIZE 8;      dict id size

section .text

global find_word

;Принимает два аргумента:
;Указатель на нуль-терминированную строку.
;Указатель на начало словаря.
;Проходит по всему словарю в поисках подходящего ключа. Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.
find_word:
    push rbx
    push r12
    mov rbx, rdi;       saving rdi before func call
    mov r12, rsi;       saving rsi before func call
    .iteration:
        test r12, r12;          if node == 0 go to break
        je .break
        mov rdi, rbx
        mov rsi, r12
        add rsi, ID_SIZE;       pointer to key (skip id)
        call string_equals;     is string equals?
        test rax, rax;          if not zero go to break
        jne .break
        mov r12, [r12];         next dict elem -> r12
        jmp .iteration
    .break:
        mov rax, r12;           addr or 0 -> rax
        pop r12
        pop rbx;                clear stack
        ret

        