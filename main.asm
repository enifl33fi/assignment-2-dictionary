%include "words.inc"
%include "dict.inc"
%include "lib.inc"

%define BUFF_SIZE 255;      buffer capacity

%define ID_SIZE 8;      dict id size


section .bss
input_buff: resb BUFF_SIZE;       255 byte reserve

section .rodata
key_too_long_err: db "Given key is too long :(", 0
dict_end_err: db "Given key not found :(", 0

section .text
global _start

_start:
    mov rdi, input_buff;        buff pointer -> rdi
    mov rsi, BUFF_SIZE;         buff size -> rsi
    call read_word;             read input
    test rax, rax;              is rax == 0?
    je .too_long;
    push rdx;                   save str length
    mov rdi, rax;               string pointer -> rdi
    mov rsi, head_node;         dict head -> rsi
    call find_word
    pop rdx;                    str length
    test rax, rax;              is rax == 0?
    je .not_found

    add rax, ID_SIZE;           pointer to key (skip id)
    add rax, rdx;               pointer + str length
    inc rax;                    skip 0 terminator
    mov rdi, rax
    call print_string
    call print_newline
    .fin:
        xor rdi, rdi
        call exit
    
    .too_long:
        mov rdi, key_too_long_err
        jmp .handle_error

    .not_found:
        mov rdi, dict_end_err
    
    .handle_error:
        call print_error
        call print_newline
        jmp .fin
    


