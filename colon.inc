%define head_node 0;        head node
%ifmacro colon 2;           is macro present?
    %error "Marco with name colon and 2 parameters already exists"
%else
    %macro colon 2
        %ifstr %1;          if first argument is string
            %ifid %2;       && second is id
                %2:
                    dq head_node;       link cur node and previous head
                    db %1, 0;           zero terminated string as key
                %define head_node %2;   redefine head node
            %else
                %error "Second parameter shoud be identifier"
            %endif
        %else
            %error "First parameter shoud be string"
        %endif
    %endmacro
%endif