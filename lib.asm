; variables
%define STR_TERMINATOR 0;

%define SYS_EXIT 60; exit system call
%define SYS_WRITE 1; write system call

%define STDERR 2; system standart error output
%define STDOUT 1; system standart output
%define STDIN 0;  system standart input

%define ASCII_HT 0x9; horizontal tab
%define ASCII_LF 0xA; line feed
%define ASCII_SPACE 0x20; space


section .text

global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT;      exit system code -> rax 
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, rdi;                               msg start -> rax
    .loop:
        cmp byte [rdi], STR_TERMINATOR;         is curr byte equals string terminator
        je .end_loop
        inc rdi;                                next byte
        jmp .loop
    .end_loop:
        sub rdi, rax;                           msg end - msg start
        mov rax, rdi;                           ans -> rax
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi;               save rdi before calling function
    call string_length;     str length -> rax
    mov rdx, rax;           str length -> rdx (syscall arg)
    mov rax, SYS_WRITE;     write system code -> rax
    pop rsi;                string start from stack -> rsi (syscall arg)
    mov rdi, STDOUT;        standart output -> rdi (syscall arg)
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    push rdi;               save rdi before calling function
    call string_length;     str length -> rax
    mov rdx, rax;           str length -> rdx (syscall arg)
    mov rax, SYS_WRITE;     write system code -> rax
    pop rsi;                string start from stack -> rsi (syscall arg)
    mov rdi, STDERR;        error output -> rdi (syscall arg)
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ASCII_LF

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp;           rdi pointer -> rsi
    mov rax, SYS_WRITE;     write system code -> rax
    mov rdx, 1;             lehgth for 1 char -> rdx
    mov rdi, STDOUT;        standart output -> rdi (syscall arg)
    syscall
    pop rdi;                clear stack
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi;        number -> rax
    test rax, rax
    jnl .positive;       if not less than zero go to pos
    .negative:
        mov rdi, '-';    print minus;
        push rax
        call print_char
        pop rax
        neg rax
    .positive:
        mov rdi, rax;    number -> rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    mov rbx, rsp;                    saving rsp
    dec rsp;                         update rsp
    mov byte[rsp], STR_TERMINATOR;   string terminator -> stack
    mov rax, rdi;                    number -> rax
    mov rcx, 10;                     divider -> rcx
    .division:
        xor rdx, rdx;           clear rdx
        div rcx;                rax//10 -> rax, rax%10->rdx
        add dl, '0';            number -> ascii
        dec rsp;                update rsp
        mov byte[rsp], dl;      num -> stack
        test rax, rax;          rax = 0?
        jne .division
    .end_division:
        mov rdi, rsp;           number pointer -> rsi
        call print_string
        mov rsp, rbx;           recover rsp
        pop rbx;                recover rbx
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax;       clear rax
    xor rcx, rcx;       counter reset
    .check:
        mov al, byte[rdi + rcx];       first char -> rax
        cmp al, byte[rsi + rcx]
        jne .differ;                is rax == second char?
        test al, al;                is rax == 0?
        je .equal
        inc rcx;                    increment counter
        jmp .check
    .differ:
        xor al, al
        ret
    .equal:
        mov al, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax;       read code -> rax
    push rax
    mov rsi, rsp;       rax pointer -> rsi
    mov rdx, 1;         size -> rdx
    mov rdi, STDIN;     standart input -> rdi
    syscall
    pop rax;            clear stack
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    mov r12, rdi;       buff pointer -> r12
    push r13
    mov r13, rsi;       buff size -> r13;
    push r14
    xor r14, r14;       string cur size
    .white_spaces:
        call read_char;       char -> rax
        cmp rax, ASCII_HT;     rax == horizontal tab?
        je .white_spaces
        cmp rax, ASCII_LF;     rax == line feed?
        je .white_spaces
        cmp rax, ASCII_SPACE;  rax == space?
        je .white_spaces
    .reading:
        test rax, rax;         rax == 0?
        je .success
        cmp rax, ASCII_HT;     rax == horizontal tab?
        je .success
        cmp rax, ASCII_LF;     rax == line feed?
        je .success
        cmp rax, ASCII_SPACE;  rax == space?
        je .success
        inc r14;               increment string size
        cmp r13, r14;          r13 <= r14;
        jle .failure;
        mov byte[r12 + r14 - 1], al; char -> buffer
        call read_char;        char -> rax
        jmp .reading
    .success:
        mov byte[r12 + r14], 0; 0 terminator -> buffer
        mov rax, r12;           buff pointer -> rax
        mov rdx, r14;           string size -> rdx
        pop r14
        pop r13
        pop r12
        ret
    .failure:
        xor rax, rax;       0 -> rax
        pop r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax;       clear rax
    xor rdx, rdx;       clear rdx
    xor r8, r8;         clear cur length
    mov rcx, 10;        multiplier -> rcx;
    .parsing:
        mov sil, byte[rdi + r8];        cur num -> rsi
        cmp sil, '0';                   0 <= num <= 9
        jl .end_parsing
        cmp sil, '9'
        jg .end_parsing
        mul rcx;                        rax *= 10
        sub sil, '0';
        add al, sil;                    rax += cur num
        inc r8;                         increment counter
        jmp .parsing
    .end_parsing:
        mov rdx, r8;        cur len -> rdx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov sil, [rdi];            probably sign -> rsi
    cmp sil, '-';              if '-' or '+' sign_care
    je .sign_care_minus
    cmp sil, '+'
    je .sign_care_plus
    jmp parse_uint
    .sign_care_minus:
        inc rdi;               ignore '-'
        call parse_uint
        neg rax;
        jmp .counting
    .sign_care_plus:
        call parse_uint
    .counting:
        test rdx, rdx;         rdx != 0
        je .break
        inc rdx;               count sign
    .break:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax;                       clear counter
    .copying:
        mov cl, byte[rdi + rax];        char -> rcx
        inc rax;                        counter += 1
        cmp rax, rdx;                   buff size < str size
        jg .failure
        mov byte[rsi + rax - 1], cl;    char -> buff
        test cl, cl;                    char == 0?
        je .success
        jmp .copying
    
    .failure:
        xor rax, rax;                   clear counter
    .success:
        ret