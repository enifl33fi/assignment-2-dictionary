ASM=nasm
ASMFLAGS=-f elf64

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o lib.o lib.asm

dict.o: dict.asm lib.inc
	$(ASM) $(ASMFLAGS) -o dict.o dict.asm

main.o: main.asm lib.inc dict.inc
	$(ASM) $(ASMFLAGS) -o main.o main.asm

program: lib.o dict.o main.o
	ld -o program lib.o dict.o main.o
	rm *.o

.PHONY: test
test:
	python3 test.py
