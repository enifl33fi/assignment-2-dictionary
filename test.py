
from subprocess import *


inputs: list = ["anek1", "anek2", "anek3", "anek4", "If/you/can/keep/your/head/when/all/about/you/Are/losing/theirs/and/blaming/it/on/you/If/you/can/trust/yourself/when/all/men/doubt/you/But/make/allowance/for/their/doubting/too/If/you/can/wait/and/not/be/tired/by/waiting/Or/being/lied/about/don't/deal/in/lies"]
outputs: list = ["1: about husband and dumplings", "2: about bear and burning car", "3: about man and hat", "", ""]
errors: list = ["", "", "", "Given key not found :(", "Given key is too long :("]

def build_program() -> None:
    process: CompletedProcess = run(["make", "program"], stdout=DEVNULL, stderr=DEVNULL)
    try:
        process.check_returncode()
        print("Compiled successfully.")
    except CalledProcessError:
        print(f"Compilation finished with code {process.returncode}.")
        exit()

def test() -> None:
    failed_count: int = 0
    tests_count: int = len(inputs)
    
    build_program()

    for i in range(len(inputs)):
        p = Popen(['./program'], stdin=PIPE, stdout=PIPE, stderr = PIPE, text=True)
        output, error = map(lambda out: out.strip(), p.communicate(input = inputs[i]))
        if output == outputs[i] and error == errors[i]:
            print(f"Tets {i + 1}... OK.")
        else:
            failed_count += 1
            if (outputs[i]):
                print(f"Test {i + 1}... failed. \nExpected output '{outputs[i]}' when given output is '{output}'.")
            else:
                print(f"Test {i + 1}... failed. \nExpected error '{errors[i]}' when given output is '{error}'.")
    print("-" * 20)
    if (failed_count):
        print(f"Failed {failed_count} tests out of {tests_count}.")
    else:
        print(f"All test passed.")


test()